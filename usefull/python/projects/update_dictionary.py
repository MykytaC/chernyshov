def update_dictionary(d, key, value):
  if key in d:
      d[key].append(value)
  elif key not in d:
      d[key] = [value]
#<<the dictionary updaty function>>

###########START############
key_list = list(my_dict.keys())
val_list = list(my_dict.values())
# list out keys and values separately


#####################START##############
def get_key(val):
    for key, value in my_dict.items():
        if val == value:
            return key
    return "key doesn't exist"
# function to return key for any value

#############################START#######################
dictionary = {'aaa': [3, 7], 'sss': [11, 23], 'dd': [14, 26, 29], 'ffff': [19]}
for value in dictionary.values():
    print(len(value))
#print the number of words in the text

######################START###################
text_by_words = []
word=''
alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
            'y', 'z','Q', 'Y', 'I', 'K', 'F', 'S', 'G', 'Z', 'H', 'W',
            'E', 'R', 'J', 'T', 'L', 'X', 'M', 'V', 'U', 'B', 'O', 'A',
            'N', 'C', 'P', 'D'}
for i in initial_text:
    for j in i:
        if j in alphabet:
            word += j.lower()
        else:
            text_by_words+=[word]
            word = ''
#<<fractioned the text by words inn lower case>>

###################START#################
with open('dataset_3363_2.txt', 'r') as file:
    for line in file:
        line = line.strip()
        initial_text += [line]
#text has been saved in the array 'initial_text'

##################START####################
import requests

r = requests.get('http://example.com')
print(r.text)

url = 'http://example.com'
par = {'key1': 'value1', 'key2': 'value2', 'key3': 'value3'}
r = requests.get(url, params = par) #passing the parameters into the request
print(r.url) #formed URL-adres with parameters GET request
print(r.text)
print(r.content)

url = 'http://httpbin.org/cookies'
cookies = {'cookies_are': 'working'}
r = requests.get(url, cookies = cookies) #sending cookies to the server
print(r.text)

print(r.cookies['example_cookie_name']) #using the recieved from server cookies
#request forming module


###################START######################
#Provide a FUNCTION to calculate an area for some shapes: square, rectangle, or circle.
def get_user_param(prompt_tex=''):
    """Get parameter from user."""
    value = input(prompt_tex)
    if value.isdigit():
        return int(value)
    else:
        print(f'{value} is not a number')

def square_area(side_length):
    """Calculate area for square."""
    return side_length ** 2

def rectangle_area(lenght, width):
    """Calculate area fro rectangle."""
    return length * width

def circle_area(radius):
    """Calculate area for circle."""
    return 3.14 * r ** 2

shape_type = input('Please, provide a shape you want to calculate area: ')

if shape_type == 'square':
    a = get_user_param('side length:')
    print(f'square area: {square_area(a)}')

elif shape_type == 'rectangle':
    a = get_user_param('lenght:')
    b = get_user_param('width:')
    print(f'rectangle area: {rectangle_area(a, b)}')

elif shape_type == 'circle':
    r = get_user_param('radius:')
    print(f'circle area: {circle_area(r)}')

else:
    print(f"I don't know {shape_type} shape :-(")