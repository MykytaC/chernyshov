text = input()
encrypting = input()
text_for_encrypting = input()
text_for_decrypting = input()
encrypted_dic = {}

def update_dictionary(d, key, value):
    if key in d:
        d[key].append(value)
    elif key not in d:
        d[key] = value

def get_key(val):
    for key, value in encrypted_dic.items():
        if val == value:
            return key
    return "key doesn't exist"

count = 0
for i in text:
    update_dictionary(encrypted_dic, i, encrypting[count])
    count += 1
for j in text_for_encrypting:
    print(encrypted_dic[j], end='')
print()
for k in text_for_decrypting:
    print(get_key(k), end='')
