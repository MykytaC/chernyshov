inf = open('dataset_3363_2.txt', 'r')
line = inf.readline()+' '
new = str(' ')
inf.close()
alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g',
            'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z','Q', 'Y', 'I', 'K', 'F', 'S', 'G',
             'Z', 'H', 'W', 'E', 'R', 'J', 'T',
             'L', 'X', 'M', 'V', 'U', 'B', 'O',
             'A', 'N', 'C', 'P', 'D'}
letters = []
numbers = []
num = ''
final = ''
count = 0
for i in range(len(line)):
    if line[i] in alphabet:
        letters += line[i]
        count += 1
        while line[count] != ' ' and line[count] not in alphabet:
            num += line[count]
            count += 1
        numbers += [num]
        num = ''
count = 0
for num in numbers:
    num = int(num)
    final += letters[count] * num
    count += 1
print(final)
with open ('dataset_3363_2.txt', 'w') as ouf:
    new = ouf.write(final)
