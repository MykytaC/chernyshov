initial_text = []

with open('dataset_3363_2.txt', 'r') as file:
    for line in file:
        line = line.strip()
        initial_text += [line]
#text has been saved in the array 'initial_text'

text_by_words = []
word=''
alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
            'y', 'z','Q', 'Y', 'I', 'K', 'F', 'S', 'G', 'Z', 'H', 'W',
            'E', 'R', 'J', 'T', 'L', 'X', 'M', 'V', 'U', 'B', 'O', 'A',
            'N', 'C', 'P', 'D'}

for i in initial_text:
    for j in i:
        if j in alphabet:
            word += j.lower()
        else:
            text_by_words+=[word]
            word = ''
#text has been saved word by word in array 'text_by_word'

words_count_dictionary = {} #the keys will be words and values will contain the ZEROES arrays
def update_dictionary(d, key, value):
  if key in d:
      d[key].append(value)
  elif key not in d:
      d[key] = [value]

for i in text_by_words:
    update_dictionary(words_count_dictionary,i,0)

# count the values for each key
most_used_word_value = 0
for key, value in words_count_dictionary.items():
    if key == '':
        continue
    else:
        if most_used_word_value < len(value):
            most_used_word_value = len(value)

most_used_word = 'y'
set_of_most_recent_used_words = set()
for k, val in words_count_dictionary.items():
    if len(val) == most_used_word_value:
        set_of_most_recent_used_words.add(k)
        for i in set_of_most_recent_used_words:
            if most_used_word>i:
                most_used_word = i
print(most_used_word, most_used_word_value)

#second variant
with open('dataset_3363_3.txt') as inf:
    e = 0
    c = ''
    for j in inf:
        s = j.lower().strip().split()
        for i in s:
            if e < s.count(i):
                e = s.count(i)
                c = i
a = c + ' ' + str(e)
with open('task_3_out.txt', 'w') as ouf:
    ouf.write(a)