steps = [input().lower() for i in range(int(input()))]
start = [0, 0]
for direction in steps:
    coordinates = direction.split()
    for i in range(2):
        if coordinates[i] == 'север':
            start[i+1] += int(coordinates[i+1])
        elif coordinates[i] == 'юг':
            start[i+1] -= int(coordinates[i+1])
        elif coordinates[i] == 'восток':
            start[i] += int(coordinates[i+1])
        elif coordinates[i] == 'запад':
            start[i] -= int(coordinates[i+1])
print(*start)
