n = int(input())
team_name = {}

def update_dictionary(d, key, value):
  if key in d:
      d[key].append(value)
  elif key not in d:
      d[key] = [value]

for i in range (n):
    results = input().split(';')
    if int(results[1]) > int(results[3]):
        update_dictionary(team_name,results[0],3)
        update_dictionary(team_name, results[2], 0)
    elif int(results[1]) < int(results[3]):
        update_dictionary(team_name,results[2],3)
        update_dictionary(team_name, results[0], 0)
    else:
        update_dictionary(team_name,results[2],1)
        update_dictionary(team_name,results[0],1)

ttl_games = victory = defeat = draw = ttl_points = 0
for kye, value in team_name.items():
    for val in value:
        if val == 3:
            victory +=1
            ttl_points+=3
            ttl_games +=1
        elif val == 0:
            defeat += 1
            ttl_games += 1
        else:
            ttl_games +=1
            draw +=1
            ttl_points +=1
    print(kye+':',ttl_games,victory,draw,defeat,ttl_points)
    ttl_games = victory = defeat = draw = ttl_points = 0



