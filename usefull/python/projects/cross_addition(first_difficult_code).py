start_array = []
k = []
work_array = []
product_array = []
rows = 0
elem = 0

while True:
    if start_array == ['end']:
        k.remove('end')
        elem -= 1
        rows -= 1
        columns = int(elem / rows)
        work_array = [[0 for j in range(columns)] for i in range(rows)]
        index = 0
        for i in range(rows):
            for j in range(columns):
                work_array[i][j] = k[index]
                index += 1

        product_array = [[0 for j in range(columns)] for i in range(rows)]

        for i in range(rows - 1):  # rows
            for j in range(columns - 1):  # columns
                product_array[i][j] = int(work_array[i][j + 1]) + \
                                      int(work_array[i + 1][j]) + \
                                      int(work_array[i - 1][j]) + \
                                      int(work_array[i][j - 1])

        for i in range(rows - 1):  # rows
            product_array[i][columns - 1] = int(work_array[i - 1][columns - 1]) + \
                                            int(work_array[i + 1][columns - 1]) + \
                                            int(work_array[i][0]) + \
                                            int(work_array[i][columns - 2])
        for j in range(columns - 1):
            product_array[rows - 1][j] = int(work_array[rows - 2][j]) + \
                                         int(work_array[0][j]) + \
                                         int(work_array[rows - 1][j + 1]) +\
                                         int(work_array[rows - 1][j - 1])

        product_array[rows - 1][columns - 1] = int(work_array[0][columns - 1]) +\
                                               int(work_array[rows - 2][columns - 1]) +\
                                               int(work_array[rows - 1][0]) +\
                                               int(work_array[rows - 1][columns - 2])

        print(*product_array, sep='\n')
        print()
        for i in range(len(product_array)):  # len(A) - возвращает количество строк в матрице А
            for j in range(len(product_array[i])):  # len(A[i]) - возвращает количество элементов в строке i
                print(product_array[i][j], end=' ')
            print()  # делаем переход на новую строку
        break
    else:
        start_array = [i for i in input().split()]
        rows += 1
        for j in start_array:
            k = k + [j]
            elem += 1
