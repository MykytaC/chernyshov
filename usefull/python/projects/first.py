'''
from pylab import *

x = linspace(0, 5, 10)
y = x ** 2

print(x, y)

figure()
plot (x, y, 'g')
xlabel('x')
ylabel('y')
title('first')
show()


fig = plt.figure()
axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])

axes.plot(x, y, 'b')
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_title('grafon')


print((6+9)//2, (9+5)//2)

'import this'

import keyword
print(keyword.kwlist)


help("keywords")


print("""This is a paragraph. 
It is made up of multiple lines and sentences.""")


a = 10
def foo():
    pass
class Bar:
    pass
print(type(a))
print(type(foo))
print(type(Bar))

import random
x = 0
y = 6
print(random.randint(x, y))
print(random.randint(x, y))
print(random.randint(x, y))


def expression(a,b):
    result = (12 * a + 25 * b) / (1 + a**(2**b))
    return round(result,2)

print (expression(2,3))

my_operation = "read"
match my_operation:
    case "read":
        print("perform read operation…")
    case "update":
        print("perform update operation …")
    case "insert":
        print("perform insert operation …")
    case "delete":
        print("perform delete operation …")
    case _:
        print("wrong variant if operation !!!")

x = int(input("Imput integer number: "))
is_prime = True
div = 2

while div < x:
    if not x % div:
        is_prime = False
        break
    div += 1

if is_prime:
    print("Prime")
else:
    print("Not prime")

'''

print("This is \"double quotes\" in double quotes")
print('This is \'single quotes\' in single quotes')
print("String without a new \n line")
print("Lorem ipsum, dolor sit amet, \
sed do eiusmod tempor incididunt ut.")


a_b = '1/3'
c_b = '5/3'
def get_fractions(a_b, c_b):
    first_value = a_b.split('/')
    second_value = c_b.split('/')
    numerator = int(first_value[0])+int(second_value[0])
    return (str(numerator) + '/' + first_value[1])


def get_longest_word(sentence):
    words = sentence.split()
    londest_word = words[0]
    for word in words:
        if len(londest_word) < len(word):
            londest_word = word
    return londest_word



some_numbers = [1, 3, 5, 9, 11, 16, 28, 44.7, 90.6, 5334]
for i in some_numbers[::2]:
    print(i)


m = [1, 2, 3, 4, 5]
for i in m:
    print(i)

d = {
    "name": "Filip",
    "age": 32,
    "is_registered": False,
    "rate": 12.5,
    "total_score": 200,
    "linked_ids": [1, 45, 98]
}
print(d["name"])
print(d.get("preferences", "There is nothing!"))

result = (20 / 2 + 12 * 2 - 9)
print(result)

def show_arguments(*args, **kwargs):
  print(args[3])
  print(kwargs['arg2'])

show_arguments(1, 'name', 'value', 10, name='arg1', arg2='Tom')
('name', 'value')