list = input() + ' ' #'aaa aaa sss dd ffff sss dd dd dd'
dictionary = {}
word = ''
list = list.lower()

def update_dictionary(d, key, value):
    if key in d:
        d[key].append(value)
    elif key not in d:
        d[key] = [value]


for i in range(len(list)):
    if list[i] == ' ':
        update_dictionary(dictionary, word, i)
        # dictionary[word] = [i]
        word = ''
    else:
        word += list[i]

for key, value in dictionary.items():
    print(key, len(value))


#second version

l = input().lower().split()
d  = {e:0 for e in l} #создаем словарь на основе списка с 0 значениями
for key in l: d[key]+= 1 #тупо считаем повторяющиеся
for key,value in d.items(): print(key, value) #тупо выводим

# third version
s = input().lower().split()
for i in set(s):
    print(i, s.count(i))
