
from typing import Dict, Any, Callable, Iterable

DataType = Iterable[Dict[str, Any]]
ModifierFunc = Callable[[DataType], DataType]


def select(*columns: str) -> ModifierFunc:
    """Return function that selects only specific columns from dataset"""

    friends = [
        {'name': 'Sam', 'gender': 'male', 'sport': 'Basketball'},
        {'name': 'Emily', 'gender': 'female', 'sport': 'volleyball'},
    ]
    list_of_dicts = []
    new_dict = {}
    for i in friends:
        for key, value in i.items():
            for name in columns:
                if key == name:
                    new_dict[key] = value
        list_of_dicts.append(new_dict)
        new_dict = {}

    return list_of_dicts


data = [
    {'name': 'Sam', 'gender': 'male', 'sport': 'Basketball'}
]
selector = [{'name': 'Sam', 'gender': 'male', 'sport': 'Basketball'}, {'name': 'Emily', 'gender': 'female', 'sport': 'volleyball'}]

query_list = []
query_dict = {}
for selector_item in selector:
    for key_selector, value_selector in selector_item.items():

        for dicts in data:
            for key_data in dicts.keys():
                if key_selector == key_data:
                    #query_dict[key_data] = value_selector
                    query_dict[key_data].append(value_selector)
    query_list.append(query_dict)
    query_dict = {}
print((query_list))

#print(select('name', 'gender', 'sport'))



filters2 = [{'sport': 'Basketball'}, {'sport': 'volleyball'}]
filters = [{'gender': 'male'}]


filtered_list = []

for filter_field in filters:
    for key_filter, value_filter in filter_field.items():
        for dicts in query_list:
            for key_query_list, value_query_list in dicts.items():
                if key_query_list == key_filter and value_query_list == value_filter:
                    filtered_list.append(dicts)


print('filtered_list', filtered_list)



def field_filter(column: str, *values: Any) -> ModifierFunc:
    """Return function that filters specific column to be one of `values`"""

    friends = [
        {'name': 'Sam', 'gender': 'male', 'sport': 'Basketball'},
        {'name': 'Emily', 'gender': 'female', 'sport': 'volleyball'},
    ]
    list_of_dicts = []
    new_dict = {}
    select_colomns = select(column)
    for filterted_dict in select_colomns:
        for field_key, field_value in filterted_dict.items():
            for value in values:
                if field_value == value:
                    new_dict[field_key] = field_value
            list_of_dicts.append(new_dict)
            new_dict = {}
    for dicts in list_of_dicts:
        if dicts == {}:
            list_of_dicts.remove(dicts)

    return list_of_dicts

print(field_filter('sport', *('Basketball', 'volleyball')))
print(field_filter(*('gender', *('male',))))