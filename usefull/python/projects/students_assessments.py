#Name;math;physics;language
#.split(;)

initial_text = []
with open('dataset_3363_2.txt', encoding='utf-8') as file:
    for line in file:
        line = line.strip().split(';')
        initial_text += [line]
#text has been saved in the array 'initial_text'

average_math = average_physics = average_lang = 0
for i in initial_text:
    print((int(i[1])+int(i[2])+int(i[3]))/3)
    average_math += int(i[1])
    average_physics += int(i[2])
    average_lang += int(i[3])
print(average_math/len(initial_text), average_physics/len(initial_text), average_lang/len(initial_text))
