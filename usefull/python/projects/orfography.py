dictionary = [input().lower() for i in range (int(input()))]
sentences = [input().lower() for i in range (int(input()))]
answer = set()
for thesis in sentences:
    check_word = thesis.split()
    for word in check_word:
        if word not in dictionary:
            answer.add(word)
print(*answer, sep='\n')
